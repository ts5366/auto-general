package com.example.demo.controller;

import com.example.demo.DemoApplication;
import com.example.demo.domain.ToDo;
import com.example.demo.service.TasksService;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.example.demo.service.ToDoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
@EnableWebMvc
@AutoConfigureMockMvc
public class ControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TasksService tasksService;

    @MockBean
    private ToDoService toDoService;

    @Test
    public void testTasksController() throws Exception {
        String input = "1234567";
        BracketsCommand command = new BracketsCommand(input);
        command.setIsBalanced(false);
        when(tasksService.validateBrackets("1234567")).thenReturn(command);
        this.mockMvc.perform(get("/tasks/validateBrackets?input=" + input)).andExpect(status().isOk())
                    .andExpect(content().json("{\"input\":\"1234567\",\"isBalanced\":false}"));
    }

    @Test
    public void testToDoController() throws Exception {
        ToDo item = new ToDo("text");
        item.setId(1L);
        String time = item.getCreatedAt();
        when(toDoService.find(1L)).thenReturn(item);
        this.mockMvc.perform(get("/todo/1")).andExpect(status().isOk())
                .andExpect(content().json("{\"id\":1,\"text\":\"text\",\"isCompleted\":false,\"createdAt\":\""+time+"\"}"));
    }


}
