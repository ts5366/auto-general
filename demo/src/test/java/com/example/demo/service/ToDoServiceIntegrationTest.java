package com.example.demo.service;

import com.example.demo.controller.ToDoPatchCommand;
import com.example.demo.domain.ToDo;
import com.example.demo.exception.NotFoundException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ToDoServiceIntegrationTest {

    @Autowired
    ToDoService toDoService;


    @Autowired
    ToDoRepo toDoRepo;

    @Before
    public void setUp() throws Exception {
        toDoRepo.deleteAll();
    }

    @After
    public void clearDown() throws Exception {
        toDoRepo.deleteAll();
    }

    @Test
    public void testFind() {
        ToDo item = new ToDo("one Sample");
        toDoRepo.save(item);
        ToDo toDo = toDoService.find(1L);
        Assert.assertEquals("one Sample", toDo.getText());
    }

    @Test(expected = NotFoundException.class)
    public void shouldThrowExceptionByFindNotExistingData() {
        toDoService.find(2L);
    }

    @Test
     public void testSave() {
        toDoService.save("saving");
        ToDo one = toDoRepo.findOne(2L);
        Assert.assertEquals("saving", one.getText());
    }

    @Test
    public void testModify() {
        toDoService.save("saving");

        ToDoPatchCommand toDoPatchCommand = new ToDoPatchCommand();
        toDoPatchCommand.setText("modifying");
        toDoPatchCommand.setIsCompleted(true);
        toDoService.modify(3L, toDoPatchCommand);

        ToDo one = toDoRepo.findOne(3L);
        Assert.assertEquals("modifying", one.getText());
    }


}
