package com.example.demo.service;

import com.example.demo.controller.BracketsCommand;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import javax.validation.ConstraintViolationException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TasksServiceTest {

    @Autowired
    TasksService tasksService;

    @Test(expected = ConstraintViolationException.class)
    public void shouldThrowExceptionByValidateInputZero() {
        tasksService.validateBrackets("");
    }

    @Test(expected = ConstraintViolationException.class)
    public void shouldThrowExceptionByExceedInputMax() {
        StringBuffer s = new StringBuffer();
        for(int i=0; i<10; i++){
            s.append("yruehieufhreree");
        }
        tasksService.validateBrackets(s.toString());
    }


    @Test
    public void shouldBeBalanced() {
        BracketsCommand command = tasksService.validateBrackets("{[dssdd]}addcd(ceeed)");
        Assert.assertEquals(true, command.getIsBalanced().booleanValue());
    }

    @Test
    public void shouldBeBalancedToo() {
        BracketsCommand command = tasksService.validateBrackets("{{[ds(([s]))dd]}addcd(c{ee}ed)}");
        Assert.assertEquals(true, command.getIsBalanced().booleanValue());
    }

    @Test
    public void shouldNotBeBalanced() {
        BracketsCommand command = tasksService.validateBrackets("{{[ds(([s]]))dd]}addcd(c{ee}ed)}");
        Assert.assertEquals(false, command.getIsBalanced().booleanValue());
    }


}
