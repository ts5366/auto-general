package com.example.demo.service;

import com.example.demo.domain.ToDo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ToDoRepo extends JpaRepository<ToDo, Long> {

    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query("UPDATE ToDo set text = ?1, isCompleted = ?2, createdAt = ?5 WHERE id = ?3 and createdAt = ?4")
    int updateToDo(String text, Boolean isCompleted, Long id, String createdAt, String now);

}
