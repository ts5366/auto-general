package com.example.demo.service;

import com.example.demo.controller.ToDoPatchCommand;
import com.example.demo.domain.ToDo;
import com.example.demo.exception.NotFoundException;
import com.example.demo.exception.UpdateFailException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;


@Service
@Slf4j
public class ToDoService {

    @Autowired
    ToDoRepo toDoRepo;

    @Transactional(readOnly = true)
    public ToDo find(Long id) {
        ToDo item = toDoRepo.findOne(id);
        if(item == null ) {
            throw new NotFoundException(id);
        }
        return item;
    }

    @Transactional
    public ToDo save(String text) {
        return toDoRepo.saveAndFlush(new ToDo(text));
    }

    @Transactional
    public ToDo modify(Long id, ToDoPatchCommand command) {
        ToDo item = toDoRepo.findOne(id);
        if(item == null ) {
            throw new NotFoundException(id);
        }

        String now = item.retrieveCurrentTime();

        int i = toDoRepo.updateToDo(command.getText(), command.getIsCompleted(), id, item.getCreatedAt(), now);

        if(i == 0) {
            throw new UpdateFailException(item.getId());
        }

        item.setIsCompleted(command.getIsCompleted());
        item.setText(command.getText());
        item.setCreatedAt(now);
        return item;
    }

}
