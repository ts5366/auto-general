package com.example.demo.service;

import com.example.demo.controller.BracketsCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;
import java.util.Stack;

@Service
@Slf4j
public class TasksService {

    private static final String PatternString = "{}[]()";

    public BracketsCommand validateBrackets(String input) {

        BracketsCommand command = new BracketsCommand(input);
        validateInput(command);
        isBalanced(command);
        return command;
    }

    private void isBalanced(BracketsCommand command) {
        Stack<Character> stack = new Stack<>();
        Character leftBracket = null;

        for (Character chara : command.getInput().toCharArray()) {
            if (!stack.isEmpty()) {
                leftBracket = stack.peek();
            }

            if(PatternString.contains(chara.toString())) {
                stack.push(chara);
            }

            if (!stack.isEmpty() && stack.size() > 1) {
                if ((leftBracket == '[' && stack.peek() == ']') ||
                        (leftBracket == '{' && stack.peek() == '}') ||
                        (leftBracket == '(' && stack.peek() == ')')) {
                    stack.pop();
                    stack.pop();
                }
            }
        }

        command.setIsBalanced(stack.isEmpty());

    }


    private void validateInput(BracketsCommand command) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<BracketsCommand>> violations = validator.validate(command);
        if(!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }

    }

}
