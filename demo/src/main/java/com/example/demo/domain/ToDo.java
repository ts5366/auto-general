package com.example.demo.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

@Data
@Entity
@Table(name="todo")
public class ToDo {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column
    private Long id;

    @Size(min=1, max=50, message = "Must be between 1 and 50 chars long" )
    @Column
    private String text;

    @Column
    private Boolean isCompleted;

    @Column
    private String createdAt;

    public ToDo() {

    }

    public ToDo(String text) {
        this.text = text;
        this.isCompleted = false;
        this.createdAt = retrieveCurrentTime();
    }



    public String retrieveCurrentTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(new Date());
    }
}