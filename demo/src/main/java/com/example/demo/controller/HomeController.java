package com.example.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.session.ExpiringSession;
import org.springframework.session.FindByIndexNameSessionRepository;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping
public class HomeController {

    @Autowired
    FindByIndexNameSessionRepository<? extends ExpiringSession> sessionRepository;

    //get
    @GetMapping(value = "/", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> home() {
        log.debug("home");
        return ResponseEntity.ok("Welcome Home");
    }


    @GetMapping(value = "/saml/CheckAuth", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> saml(HttpServletRequest request, HttpSession session) {
        log.debug("saml");
        Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                System.out.println("cookie: " + cookie.getName() + " : " + cookie.getValue());
            }
        }
        return ResponseEntity.ok("Welcome Auto General");
    }

    @RequestMapping(value="/cookie", produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> cookie(@RequestParam("browser") String browser, HttpServletRequest request, HttpSession session, HttpServletResponse response) throws IOException {
        //取出session中的browser
        Object sessionBrowser = session.getAttribute("browser");
        if (sessionBrowser == null) {
            System.out.println("不存在session，设置browser=" + browser);
            session.setAttribute("browser", browser);
        } else {
            System.out.println("存在session，browser=" + sessionBrowser.toString());
        }
        Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                System.out.println("cookie: " + cookie.getName() + " : " + cookie.getValue());
            }
        }
        //return ResponseEntity.ok("Welcome Cookie");
        response.sendRedirect("/");
        return null;
    }

}