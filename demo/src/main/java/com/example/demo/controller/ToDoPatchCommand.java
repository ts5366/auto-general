package com.example.demo.controller;

import lombok.Data;


@Data
public class ToDoPatchCommand {
    private String text;
    private Boolean isCompleted;
}
