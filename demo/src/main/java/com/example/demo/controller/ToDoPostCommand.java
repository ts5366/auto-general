package com.example.demo.controller;

import lombok.Data;



@Data
public class ToDoPostCommand {
    private String text;
}
