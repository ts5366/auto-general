package com.example.demo.controller;

import com.example.demo.service.TasksService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@Slf4j
@RequestMapping(value = "/tasks", produces = "application/json;charset=UTF-8")
public class TasksController {

    @Autowired
    TasksService tasksService;

    //get
    @GetMapping("/validateBrackets")
    public ResponseEntity<BracketsCommand> validateBrackets(@RequestParam(value = "input", required = true)  String input) {
        log.debug(input);
        BracketsCommand command = tasksService.validateBrackets(input);
        return ResponseEntity.ok(command);
    }



}
