package com.example.demo.controller;

import com.example.demo.domain.ToDo;
import com.example.demo.service.ToDoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
@Slf4j
@RequestMapping(value = "/todo", produces = "application/json;charset=UTF-8")
public class ToDoController {

    @Autowired
    ToDoService service;

    //post body required
    @PostMapping
    public ResponseEntity<ToDo> createToDo(@RequestBody(required = true) @Validated ToDoPostCommand command ) {
        log.debug(command.toString());
        ToDo item = service.save(command.getText());
        return ResponseEntity.ok(item);
    }

    //get id required
    @GetMapping("/{id}")
    public  ResponseEntity<ToDo> getToDo(@PathVariable(required = true) Long id) {
        log.debug(id.toString());
        ToDo item = service.find(id);
        return ResponseEntity.ok(item);
    }

    //modify id/body reuqired
    @PatchMapping("/{id}")
    public ResponseEntity<ToDo> modifyToDo(@RequestBody(required = true) @Validated ToDoPatchCommand command,
                                           @PathVariable(required = true) Long id) {
        log.debug("not sleeping");
        log.debug("id was " + id.toString());
        log.debug(command.toString());
        ToDo item = service.modify(id, command);
        return ResponseEntity.ok(item);
    }

}
