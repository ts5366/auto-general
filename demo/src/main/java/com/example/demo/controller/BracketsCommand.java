package com.example.demo.controller;

import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class BracketsCommand {

    @Size(min=1, max=50, message = "Must be between 1 and 50 chars long" )
    private String input;
    private Boolean isBalanced;


    public BracketsCommand(String input) {
        this.input = input;
    }
}
