package com.example.demo.exception;

public class NotFoundException extends BusinessException {

    public NotFoundException(Long id) {
        super("1000", "Item with " + id + " not found");
    }
}
