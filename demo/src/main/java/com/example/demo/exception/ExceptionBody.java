package com.example.demo.exception;

import lombok.Data;
import java.util.ArrayList;
import java.util.List;


@Data
public class ExceptionBody {
    private List detailsList = new ArrayList<>();
    private String name;

    public ExceptionBody(String name) {
        this.name = name;
    }

    @Data
    public static class ExceptionDetails {
        private String location;
        private String param;
        private String msg;
        private String value;

    }

    @Data
    public static class ExceptionMessage {
        private String message;
    }
}
