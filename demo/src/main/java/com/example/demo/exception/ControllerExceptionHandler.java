package com.example.demo.exception;


import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
@Slf4j
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {ConstraintViolationException.class})
    protected ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException ex, WebRequest request) {
        log.error("validate error occurred for access [{}].", request.toString(), ex);
        ExceptionBody body = new ExceptionBody("ValidationError");

        for(ConstraintViolation violation : ex.getConstraintViolations()) {
            ExceptionBody.ExceptionDetails details = new ExceptionBody.ExceptionDetails();

            details.setValue(violation.getInvalidValue().toString());
            details.setLocation("params");
            details.setMsg(violation.getMessageTemplate());
            details.setParam(violation.getPropertyPath().toString());

            body.getDetailsList().add(details);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
    }

    @ExceptionHandler(value = {NotFoundException.class})
    protected ResponseEntity<Object> handleNotFoundException(NotFoundException ex, WebRequest request) {
        log.error("validate error occurred for access [{}].", request.toString(), ex);
        ExceptionBody body = new ExceptionBody("NotFoundError");
        ExceptionBody.ExceptionMessage message = new ExceptionBody.ExceptionMessage();
        message.setMessage(ex.getMessage());
        body.getDetailsList().add(message);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
    }

    @ExceptionHandler(value = {UpdateFailException.class})
    protected ResponseEntity<Object> handleUpdateFailException(UpdateFailException ex, WebRequest request) {
        log.error("validate error occurred for access [{}].", request.toString(), ex);
        ExceptionBody body = new ExceptionBody("UpdateFailError");
        ExceptionBody.ExceptionMessage message = new ExceptionBody.ExceptionMessage();
        message.setMessage(ex.getMessage());
        body.getDetailsList().add(message);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
    }

}
