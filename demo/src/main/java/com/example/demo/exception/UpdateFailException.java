package com.example.demo.exception;


public class UpdateFailException extends BusinessException {

    public UpdateFailException(Long id) {
        super("2000", "Item with " + id + " update fail");
    }
}